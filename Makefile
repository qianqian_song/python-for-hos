# MIT License
#
# Copyright (c) 2021 唐佐林 
# WeChat : delphi_tang
# EMail: delphi_tang@dt4sw.com
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

PYPATH  = py
LIBPATH = lib
DEVPATH = dev
PROPATH = /path/to/code-v1.1/root
CMPPATH = /path/compiler/root/bin
CMPPREFIX = riscv32-unknown-elf-

include ./py/mkenv.mk

include $(DEVPATH)/setting.mk

QSTR_DEFS += qstrdefsport.h

MICROPY_ROM_TEXT_COMPRESSION ?= 1

include ./py/py.mk

CROSS_COMPILE = $(CMPPATH)/$(CMPPREFIX)

INC += -I.
INC += -I$(TOP)
INC += -I$(BUILD)
INC += -I$(LIBPATH)
INC += -I$(PYPATH)
INC += -I$(DEVPATH)

CFLAGS += $(INC) -Wall -Werror -std=gnu99  
CFLAGS += -Os -DNDEBUG
CFLAGS += -fdata-sections -ffunction-sections

ARFLAGS = rc

CSUPEROPT = -Os

LIBS = 

SRC_C = \
	$(wildcard $(LIBPATH)/utils/*.c) \
	$(wildcard $(DEVPATH)/*.c) \
	dtpython.c

OBJ = $(PY_CORE_O) $(addprefix $(BUILD)/, $(SRC_C:.c=.o))

all: $(BUILD)/libdtpython.a

$(BUILD)/libdtpython.a: $(OBJ)
	$(ECHO) "ARCHIVE $@"
	$(Q)$(AR) $(ARFLAGS) -o $@ $^ $(LIBS)
	$(ECHO) "Success! Target ==> $@"

include $(TOP)/py/mkrules.mk
