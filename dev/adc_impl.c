/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include "adc_impl.h"
#include "adc_ex.h"

#define ADC_CHANNEL_NUM   8

typedef struct
{
    int model;
    int curbais;
    int count;
} adc_cfg;

static adc_cfg g_config[ADC_CHANNEL_NUM] = {0};

mp_obj_t mp_adc_set_bais(mp_obj_t index, mp_obj_t bais)
{
    unsigned int channel = mp_obj_get_int(index);
    int cur_bais = mp_obj_get_int(bais);
    int r = 0;
    
    if( channel < ADC_CHANNEL_NUM )
    {
        g_config[channel].curbais = cur_bais;
    }
    else
    {
        r = -1;
    }
    
    return mp_obj_new_int(r);
}

mp_obj_t mp_adc_set_model(mp_obj_t index, mp_obj_t model)
{
    unsigned int channel = mp_obj_get_int(index);
    int equ_model = mp_obj_get_int(model);
    int r = 0;
    
    if( channel < ADC_CHANNEL_NUM )
    {
        g_config[channel].model = equ_model;
    }
    else
    {
        r = -1;
    }
    
    return mp_obj_new_int(r);
}

mp_obj_t mp_adc_set_reset_count(mp_obj_t index, mp_obj_t count)
{
    unsigned int channel = mp_obj_get_int(index);
    unsigned short rstcnt = (unsigned short)mp_obj_get_int(count);
    int r = 0;
    
    if( channel < ADC_CHANNEL_NUM )
    {
        g_config[channel].count = rstcnt;
    }
    else
    {
        r = -1;
    }
    
    return mp_obj_new_int(r);
}

mp_obj_t mp_adc_read(mp_obj_t index)
{
    unsigned int channel = mp_obj_get_int(index);
    unsigned int r = -1;
    unsigned short data = 0;
    mp_obj_t ret[2] = 
    {
        mp_obj_new_int(r),
        mp_obj_new_int(data),
    };
    
    if( channel < ADC_CHANNEL_NUM )
    {
        r = DTAdcRead(channel,
                       &data, 
                       g_config[channel].model,
                       g_config[channel].curbais,
                       g_config[channel].count);
                                            
        ret[0] = mp_obj_new_int(r);
        ret[1] = mp_obj_new_int(data);
    }
    else
    {
        r = -1;
    }
        
    return mp_obj_new_tuple(2, ret);
}

mp_obj_t mp_adc_query_bais_value(mp_obj_t key)
{
    return mp_obj_new_int(adc_query_bais_value(mp_obj_str_get_str(key)));
}

mp_obj_t mp_adc_query_model_value(mp_obj_t key)
{
    return mp_obj_new_int(adc_query_model_value(mp_obj_str_get_str(key)));
}
