/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/


#include "gpio_impl.h"
#include "gpio_ex.h"

#define GPIO_IDX_MAX  16

typedef struct 
{
    int type;
    mp_obj_t func;
    mp_obj_t arg;
} mp_custom_cb_t;

static volatile mp_custom_cb_t g_callback[GPIO_IDX_MAX] = {0};

static void gpio_callback_func(char* data)
{
    int index = (size_t)data;
    
    if( g_callback[index].type )
    {
        mp_call_function_1(g_callback[index].func, g_callback[index].arg);
    }
}

mp_obj_t mp_gpio_init(mp_obj_t idx)
{
    return mp_obj_new_int(IoTGpioInit(mp_obj_get_int(idx)));
}

mp_obj_t mp_gpio_deinit(mp_obj_t idx)
{
    return mp_obj_new_int(IoTGpioDeinit(mp_obj_get_int(idx)));
}

mp_obj_t mp_gpio_get_dir(mp_obj_t idx)
{
    int index = mp_obj_get_int(idx);
    IotGpioDir dir = 0;
    unsigned int r = IoTGpioGetDir(index, &dir);
    mp_obj_t ret[2] = 
    {
        mp_obj_new_int(r),
        mp_obj_new_int(dir),
    };
    
    return mp_obj_new_tuple(2, ret);
}

mp_obj_t mp_gpio_set_dir(mp_obj_t idx, mp_obj_t dir)
{
    int index = mp_obj_get_int(idx);
    int direction = mp_obj_get_int(dir);
    unsigned int ret = IoTGpioSetDir(index, (IotGpioDir)direction);
    
    return mp_obj_new_int(ret);
}

mp_obj_t mp_gpio_get_output(mp_obj_t idx)
{
    int index = mp_obj_get_int(idx);
    IotGpioValue val = 0;
    unsigned int r = IoTGpioGetOutputVal(index, &val);
    mp_obj_t ret[2] = 
    {
        mp_obj_new_int(r),
        mp_obj_new_int(val),
    };
    
    return mp_obj_new_tuple(2, ret);
}

mp_obj_t mp_gpio_set_output(mp_obj_t idx, mp_obj_t val)
{
    int index = mp_obj_get_int(idx);
    int value = !!mp_obj_get_int(val);
    unsigned int ret = IoTGpioSetOutputVal(index, (IotGpioValue)value);
    
    return mp_obj_new_int(ret);
}

mp_obj_t mp_gpio_get_input(mp_obj_t idx)
{
    int index = mp_obj_get_int(idx);
    IotGpioValue val = 0;
    unsigned int r = IoTGpioGetInputVal(index, &val);
    mp_obj_t ret[2] = 
    {
        mp_obj_new_int(r),
        mp_obj_new_int(val),
    };
    
    return mp_obj_new_tuple(2, ret);
}

mp_obj_t mp_gpio_set_isr_mode(mp_obj_t idx, mp_obj_t mode)
{
    int index = mp_obj_get_int(idx);
    int type = mp_obj_get_int(mode);
    int r = -1;
    
    if( (0 <= index) && (index < GPIO_IDX_MAX) && (0 < type) && (type < GPIO_TYPE_MAX) )
    {
        IotGpioIntType itype = 0;
        IotGpioIntPolarity ipolarity = 0;
        
        switch( type )
        {
            case GPIO_TYPE_LEVEL_LOW:
                itype = IOT_INT_TYPE_LEVEL;
                ipolarity = IOT_GPIO_EDGE_FALL_LEVEL_LOW;
                break;
            case GPIO_TYPE_LEVEL_HIGH:
                itype = IOT_INT_TYPE_LEVEL;
                ipolarity = IOT_GPIO_EDGE_RISE_LEVEL_HIGH;
                break;
            case GPIO_TYPE_EDGE_FALL_LOW:
                itype = IOT_INT_TYPE_EDGE;
                ipolarity = IOT_GPIO_EDGE_FALL_LEVEL_LOW;
                break;
            case GPIO_TYPE_EDGE_RISE_HIGH:
                itype = IOT_INT_TYPE_EDGE;
                ipolarity = IOT_GPIO_EDGE_RISE_LEVEL_HIGH;
                break;    
        }
        
        if( (r = IoTGpioSetIsrMode(index, itype, ipolarity)) == 0 )
        {
            g_callback[index].type = type;
        }
    }
    
    return mp_obj_new_int(r);
}

mp_obj_t mp_gpio_get_isr_mode(mp_obj_t idx)
{
    int index = mp_obj_get_int(idx);
    int ret = -1;
    
    if( (0 <= index) && (index < GPIO_IDX_MAX) )
    {
        ret = g_callback[index].type;
    }
    
    return mp_obj_new_int(ret);
}

mp_obj_t mp_gpio_set_isr_mask(mp_obj_t idx, mp_obj_t msk)
{
    int index = mp_obj_get_int(idx);
    unsigned char mask = (unsigned char)mp_obj_get_int(msk);
    int r = IoTGpioSetIsrMask(index, mask);

    return mp_obj_new_int(r);
}

mp_obj_t mp_gpio_register_isr_func(mp_obj_t idx, mp_obj_t func, mp_obj_t arg)
{
    int index = mp_obj_get_int(idx);
    unsigned int r = -1;   
    const mp_obj_type_t* type = mp_obj_get_type(func);

    if( type->call )
    {
        if( (0 <= index) && (index < GPIO_IDX_MAX) && g_callback[index].type )
        {
            IotGpioIntType itype = 0;
            IotGpioIntPolarity ipolarity = 0;
            
            g_callback[index].func = func;
            g_callback[index].arg = arg;
            
            switch( g_callback[index].type )
            {
                case GPIO_TYPE_LEVEL_LOW:
                    itype = IOT_INT_TYPE_LEVEL;
                    ipolarity = IOT_GPIO_EDGE_FALL_LEVEL_LOW;
                    break;
                case GPIO_TYPE_LEVEL_HIGH:
                    itype = IOT_INT_TYPE_LEVEL;
                    ipolarity = IOT_GPIO_EDGE_RISE_LEVEL_HIGH;
                    break;
                case GPIO_TYPE_EDGE_FALL_LOW:
                    itype = IOT_INT_TYPE_EDGE;
                    ipolarity = IOT_GPIO_EDGE_FALL_LEVEL_LOW;
                    break;
                case GPIO_TYPE_EDGE_RISE_HIGH:
                    itype = IOT_INT_TYPE_EDGE;
                    ipolarity = IOT_GPIO_EDGE_RISE_LEVEL_HIGH;
                    break;    
            }
            
            r = IoTGpioRegisterIsrFunc(index, itype, ipolarity, gpio_callback_func, (char*)(size_t)index);
        }
    }
    else
    {
        mp_raise_TypeError(MP_ERROR_TEXT("func is not callable"));
    }
    
    return mp_obj_new_int(r);
}

mp_obj_t mp_gpio_unregister_isr_func(mp_obj_t idx)
{
    int index = mp_obj_get_int(idx);
    unsigned int r = -1;
    
    if( (0 <= index) && (index < GPIO_IDX_MAX) )
    {
        g_callback[index].type = 0;
        
        r = IoTGpioUnregisterIsrFunc(index);
    }
    
    return mp_obj_new_int(r);
}

mp_obj_t mp_gpio_set_pull(mp_obj_t idx, mp_obj_t val)
{
    return mp_obj_new_int(DTGpioSetPull(mp_obj_get_int(idx), mp_obj_get_int(val)));
}

mp_obj_t mp_gpio_set_func(mp_obj_t idx, mp_obj_t val)
{ 
    return mp_obj_new_int(DTGpioSetFunc(mp_obj_get_int(idx), mp_obj_get_int(val)));
}

mp_obj_t mp_gpio_query_pull_value(mp_obj_t key)
{
    return mp_obj_new_int(gpio_query_pull_value(mp_obj_str_get_str(key)));
}

mp_obj_t mp_gpio_query_func_value(mp_obj_t idx, mp_obj_t key)
{ 
    return mp_obj_new_int(gpio_query_func_value(mp_obj_get_int(idx), mp_obj_str_get_str(key)));
}
