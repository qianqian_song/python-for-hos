/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#ifndef GPIO_IMPL_H
#define GPIO_IMPL_H

#include "py/objtype.h"
#include "py/runtime.h"
#include "iot_gpio.h"

enum
{
    GPIO_TYPE_LEVEL_LOW = 1,
    GPIO_TYPE_LEVEL_HIGH,
    GPIO_TYPE_EDGE_FALL_LOW,
    GPIO_TYPE_EDGE_RISE_HIGH,
    GPIO_TYPE_MAX
};

mp_obj_t mp_gpio_init(mp_obj_t idx);
mp_obj_t mp_gpio_deinit(mp_obj_t idx);
mp_obj_t mp_gpio_get_dir(mp_obj_t idx);
mp_obj_t mp_gpio_set_dir(mp_obj_t idx, mp_obj_t dir);
mp_obj_t mp_gpio_get_output(mp_obj_t idx);
mp_obj_t mp_gpio_set_output(mp_obj_t idx, mp_obj_t val);
mp_obj_t mp_gpio_get_input(mp_obj_t idx);
mp_obj_t mp_gpio_set_pull(mp_obj_t idx, mp_obj_t val);
mp_obj_t mp_gpio_set_func(mp_obj_t idx, mp_obj_t val);
mp_obj_t mp_gpio_query_func_value(mp_obj_t idx, mp_obj_t key);
mp_obj_t mp_gpio_query_pull_value(mp_obj_t key);
mp_obj_t mp_gpio_set_isr_mode(mp_obj_t idx, mp_obj_t mode);
mp_obj_t mp_gpio_get_isr_mode(mp_obj_t idx);
mp_obj_t mp_gpio_register_isr_func(mp_obj_t idx, mp_obj_t func, mp_obj_t arg);
mp_obj_t mp_gpio_unregister_isr_func(mp_obj_t idx);
mp_obj_t mp_gpio_set_isr_mask(mp_obj_t idx, mp_obj_t mask);

#endif
