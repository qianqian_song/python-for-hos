/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include <malloc.h>
#include "mperrno.h"
#include "utility.h"
#include "i2c_impl.h"
#include "i2c_ex.h"

mp_obj_t mp_i2c_init(mp_obj_t idx, mp_obj_t baudrate)
{
    int id = mp_obj_get_int(idx);
    unsigned int br = mp_obj_get_int(baudrate);
    unsigned int r = IoTI2cInit(id, br);
    
    return mp_obj_new_int(r);
}

mp_obj_t mp_i2c_deinit(mp_obj_t idx)
{
    int id = mp_obj_get_int(idx);
    unsigned int r = IoTI2cDeinit(id);
    
    return mp_obj_new_int(r);
}

mp_obj_t mp_i2c_write(mp_obj_t idx, mp_obj_t addr, mp_obj_t data)
{
    unsigned int r = -1;
    int id = mp_obj_get_int(idx);
    int devAddr = mp_obj_get_int(addr);  
    
    if( mp_obj_is_type(data, &mp_type_list) )
    {
        unsigned char* bytes = NULL;
        size_t slen = pylist_to_integer_array(data, bytes);
        
        if( bytes )
        {       
            r = IoTI2cWrite(id, devAddr, bytes, slen);
        }
        else
        {
            mp_raise_OSError(MP_ENOMEM);
        }
        
        free(bytes);
    }
    else
    {    
        mp_raise_TypeError(MP_ERROR_TEXT("can't convert to list"));
    }
   
    return mp_obj_new_int(r);
}

mp_obj_t mp_i2c_read(mp_obj_t idx, mp_obj_t addr, mp_obj_t len)
{
    unsigned int r = -1;
    int id = mp_obj_get_int(idx);
    int devAddr = mp_obj_get_int(addr);  
    size_t rlen = mp_obj_get_int(len);  
    unsigned char* buffer = malloc(sizeof(*buffer) * rlen);
    mp_obj_t ret[2] = 
    {
        mp_obj_new_int(-1),
        mp_const_none,
    };
    
    if( buffer )
    {
        mp_obj_t pylist = mp_const_none;
        
        r = IoTI2cRead(id, devAddr, buffer, rlen);
        
        if( (r == 0) && ((pylist = integer_array_to_pylist(buffer, rlen)) != mp_const_none) )
        {           
            ret[0] = mp_obj_new_int(r);
            ret[1] = pylist;
        }
    }
    else
    {
        mp_raise_OSError(MP_ENOMEM);
    }
    
    free(buffer);
    
    return mp_obj_new_tuple(2, ret);
}

mp_obj_t mp_i2c_write_read(size_t n_args, const mp_obj_t* args)
{
    unsigned int r = -1;
    int id = mp_obj_get_int(args[0]);
    int devAddr = mp_obj_get_int(args[1]);  
    mp_obj_t ret[2] = 
    {
        mp_obj_new_int(r),
        mp_const_none,
    };
    
    if( mp_obj_is_type(args[2], &mp_type_list) )
    {
        unsigned char* bytes = NULL;
        size_t slen = pylist_to_integer_array(args[2], bytes);
        size_t rlen = mp_obj_get_int(args[3]);
        unsigned char* buffer = malloc(sizeof(*buffer) * rlen);
            
        if( bytes && buffer )
        {
            mp_obj_t pylist = mp_const_none;
 
            r = DTI2cWriteRead(id, devAddr, bytes, slen, buffer, rlen);
            
            if( (r == 0) && ((pylist = integer_array_to_pylist(buffer, rlen)) != mp_const_none) )
            {           
                ret[0] = mp_obj_new_int(r);
                ret[1] = pylist;
            }
        }
        else
        {
            mp_raise_OSError(MP_ENOMEM);
        }
        
        free(bytes);
        free(buffer);
    }
    else
    {
        mp_raise_TypeError(MP_ERROR_TEXT("can't convert to list"));
    }
    
    return mp_obj_new_tuple(2, ret);
}

mp_obj_t mp_i2c_set_baudrate(mp_obj_t idx, mp_obj_t baudrate)
{
    int id = mp_obj_get_int(idx);
    unsigned int br = mp_obj_get_int(baudrate);  
    unsigned int r = IoTI2cSetBaudrate(id, br);
    
    return mp_obj_new_int(r);
}
