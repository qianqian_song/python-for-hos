/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include "gpio_impl.h"

STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_gpio_init_obj, mp_gpio_init);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_gpio_deinit_obj, mp_gpio_deinit);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_gpio_get_dir_obj, mp_gpio_get_dir);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_gpio_set_dir_obj, mp_gpio_set_dir);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_gpio_get_output_obj, mp_gpio_get_output);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_gpio_set_output_obj, mp_gpio_set_output);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_gpio_get_input_obj, mp_gpio_get_input);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_gpio_set_pull_obj, mp_gpio_set_pull);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_gpio_set_func_obj, mp_gpio_set_func);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_gpio_query_func_value_obj, mp_gpio_query_func_value);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_gpio_query_pull_value_obj, mp_gpio_query_pull_value);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_gpio_get_isr_mode_obj, mp_gpio_get_isr_mode);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_gpio_set_isr_mode_obj, mp_gpio_set_isr_mode);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_gpio_unregister_isr_func_obj, mp_gpio_unregister_isr_func);
STATIC const MP_DEFINE_CONST_FUN_OBJ_3(mp_gpio_register_isr_func_obj, mp_gpio_register_isr_func);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_gpio_set_isr_mask_obj, mp_gpio_set_isr_mask);

STATIC const mp_rom_map_elem_t mp_module_gpio_globals_table[] = 
{
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_gpio)},
    {MP_ROM_QSTR(MP_QSTR_level_low), MP_ROM_INT(GPIO_TYPE_LEVEL_LOW)},
    {MP_ROM_QSTR(MP_QSTR_level_high), MP_ROM_INT(GPIO_TYPE_LEVEL_HIGH)},
    {MP_ROM_QSTR(MP_QSTR_fall_low), MP_ROM_INT(GPIO_TYPE_EDGE_FALL_LOW)},
    {MP_ROM_QSTR(MP_QSTR_rise_high), MP_ROM_INT(GPIO_TYPE_EDGE_RISE_HIGH)},
    {MP_ROM_QSTR(MP_QSTR_dir_in), MP_ROM_INT(IOT_GPIO_DIR_IN)},
    {MP_ROM_QSTR(MP_QSTR_dir_out), MP_ROM_INT(IOT_GPIO_DIR_OUT)},    
    {MP_ROM_QSTR(MP_QSTR_gpio_init), MP_ROM_PTR(&mp_gpio_init_obj)},
    {MP_ROM_QSTR(MP_QSTR_gpio_deinit), MP_ROM_PTR(&mp_gpio_deinit_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_dir), MP_ROM_PTR(&mp_gpio_set_dir_obj)},
    {MP_ROM_QSTR(MP_QSTR_get_dir), MP_ROM_PTR(&mp_gpio_get_dir_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_output), MP_ROM_PTR(&mp_gpio_set_output_obj)},
    {MP_ROM_QSTR(MP_QSTR_get_output), MP_ROM_PTR(&mp_gpio_get_output_obj)},
    {MP_ROM_QSTR(MP_QSTR_get_input), MP_ROM_PTR(&mp_gpio_get_input_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_pull), MP_ROM_PTR(&mp_gpio_set_pull_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_func), MP_ROM_PTR(&mp_gpio_set_func_obj)},
    {MP_ROM_QSTR(MP_QSTR_query_func_value), MP_ROM_PTR(&mp_gpio_query_func_value_obj)},
    {MP_ROM_QSTR(MP_QSTR_query_pull_value), MP_ROM_PTR(&mp_gpio_query_pull_value_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_isr_mode), MP_ROM_PTR(&mp_gpio_set_isr_mode_obj)},
    {MP_ROM_QSTR(MP_QSTR_get_isr_mode), MP_ROM_PTR(&mp_gpio_get_isr_mode_obj)},
    {MP_ROM_QSTR(MP_QSTR_register_isr_func), MP_ROM_PTR(&mp_gpio_register_isr_func_obj)},
    {MP_ROM_QSTR(MP_QSTR_unregister_isr_func), MP_ROM_PTR(&mp_gpio_unregister_isr_func_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_isr_mask), MP_ROM_PTR(&mp_gpio_set_isr_mask_obj)},
};

STATIC MP_DEFINE_CONST_DICT(mp_module_gpio_globals, mp_module_gpio_globals_table); 

const mp_obj_module_t mp_module_gpio = 
{
    .base = {&mp_type_module},    
    .globals = (mp_obj_dict_t*)&mp_module_gpio_globals,
};

MP_REGISTER_MODULE(MP_QSTR_gpio, mp_module_gpio, 1);
