/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include "i2c_impl.h"

STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_i2c_init_obj, mp_i2c_init);
STATIC const MP_DEFINE_CONST_FUN_OBJ_1(mp_i2c_deinit_obj, mp_i2c_deinit);
STATIC const MP_DEFINE_CONST_FUN_OBJ_3(mp_i2c_write_obj, mp_i2c_write);
STATIC const MP_DEFINE_CONST_FUN_OBJ_3(mp_i2c_read_obj, mp_i2c_read);
STATIC const MP_DEFINE_CONST_FUN_OBJ_2(mp_i2c_set_baudrate_obj, mp_i2c_set_baudrate);
STATIC const MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(mp_i2c_write_read_obj, 4, 4, mp_i2c_write_read);

STATIC const mp_rom_map_elem_t mp_module_i2c_globals_table[] = 
{
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_i2c)},
    {MP_ROM_QSTR(MP_QSTR_i2c_init), MP_ROM_PTR(&mp_i2c_init_obj)},
    {MP_ROM_QSTR(MP_QSTR_i2c_deinit), MP_ROM_PTR(&mp_i2c_deinit_obj)},
    {MP_ROM_QSTR(MP_QSTR_write), MP_ROM_PTR(&mp_i2c_write_obj)},
    {MP_ROM_QSTR(MP_QSTR_read), MP_ROM_PTR(&mp_i2c_read_obj)},
    {MP_ROM_QSTR(MP_QSTR_write_read), MP_ROM_PTR(&mp_i2c_write_read_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_baudrate), MP_ROM_PTR(&mp_i2c_set_baudrate_obj)},
};

STATIC MP_DEFINE_CONST_DICT(mp_module_i2c_globals, mp_module_i2c_globals_table); 

const mp_obj_module_t mp_module_i2c =
{
    .base = {&mp_type_module},    
    .globals = (mp_obj_dict_t*)&mp_module_i2c_globals,
};

MP_REGISTER_MODULE(MP_QSTR_i2c, mp_module_i2c, 1);
