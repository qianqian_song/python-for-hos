/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#ifndef SPI_EX_H
#define SPI_EX_H

typedef struct 
{
    unsigned int cpol;
    unsigned int cpha;
    unsigned int protocol;
    unsigned int dataWidth;
    unsigned int endian;
    unsigned int freq;
} SpiInfo;

unsigned int DTSpiInit(unsigned int id, SpiInfo info);
unsigned int DTSpiDeinit(unsigned int id);
unsigned int DTSpiHostWrite(unsigned int id, unsigned char* writeData, unsigned int byteLen);
unsigned int DTSpiHostRead(unsigned int id, unsigned char* readData, unsigned int byteLen);
unsigned int DTSpiHostWriteRead(unsigned int id, unsigned char* writeData, unsigned char* readData, unsigned int byteLen);
unsigned int DTSpiSetInfo(unsigned int id, SpiInfo info);

#endif
