/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#include "utility.h"

void object_print(const mp_print_t* print, mp_obj_t self_in, mp_print_kind_t kind)
{
    mp_printf(print, "<%s object at %p>", mp_obj_get_type_str(self_in), MP_OBJ_TO_PTR(self_in));
}

const type_field_info_t* find_field_info(const char* name, const type_field_info_t* iftbl, size_t size)
{
    const type_field_info_t* ret = NULL;
    int i = 0;
    
    for(i=0; i<size; i++)
    {
        if( strcmp(name, iftbl[i].name) == 0 )
        {
            ret = &iftbl[i];
            break;
        }
    }
    
    return ret;
}

int query_value(const char* key, const key_val_t* kvt, size_t len)
{
    int ret = -1;
    
    if( key && kvt )
    {
        int i = 0;
        
        for(i=0; i<len; i++)
        {
            if( strcasecmp(key, kvt[i].key) == 0 )
            {
                ret = kvt[i].val;
                break;
            }
        }
    }
    
    return ret;
}
