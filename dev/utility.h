/****************************************************************************

MIT License

Copyright (c) 2021 唐佐林
WeChat : delphi_tang
EMail: delphi_tang@dt4sw.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*****************************************************************************/

#ifndef UTILITY_H
#define UTILITY_H

#include <malloc.h>
#include "py/objtype.h"
#include "py/runtime.h"

typedef struct
{
    const char* name;
    unsigned int offset;
    unsigned int size;
    unsigned int min;
    unsigned int max;
} type_field_info_t;

typedef struct
{
    const char* key;
    const int val;
} key_val_t;

void object_print(const mp_print_t* print, mp_obj_t self_in, mp_print_kind_t kind);
const type_field_info_t* find_field_info(const char* name, const type_field_info_t* iftbl, size_t size);
int query_value(const char* key, const key_val_t* kvt, size_t len);

#define dim(a) (sizeof(a)/sizeof(*a))

#define type_offset_value(name, type, member, min, max)   {name, offsetof(type, member), sizeof(((type*)0)->member), min, max}

#define pylist_to_integer_array(pylist, pint)                 \
({                                                            \
    size_t len = 0;                                           \
    mp_obj_t* item = NULL;                                    \
    mp_obj_list_get(pylist, &len, &item);                     \
    pint = malloc(sizeof(*pint) * len);                       \
    if( pint )                                                \
    {                                                         \
        int i = 0;                                            \
        for(i=0; i<len; i++)                                  \
        {                                                     \
            pint[i] = mp_obj_get_int(item[i]);                \
        }                                                     \
    }                                                         \
    len;                                                      \
})

#define integer_array_to_pylist(pint, len)                    \
({                                                            \
    mp_obj_t* list = malloc(sizeof(*list) * len);             \
    mp_obj_t pylist = mp_const_none;                          \
    int i = 0;                                                \
    if( list )                                                \
    {                                                         \
        for(i=0; i<len; i++)                                  \
        {                                                     \
            list[i] = mp_obj_new_int(pint[i]);                \
        }                                                     \
        pylist = mp_obj_new_list(len, list);                  \
    }                                                         \
    free(list);                                               \
    pylist;                                                   \
})

#define class_name(n)     mp_##n##_t

#define class_object(name)   mp_##name##_type

#define class_type(n, t)                                      \
typedef struct                                                \
{                                                             \
    mp_obj_base_t base;                                       \
    t member;                                                 \
} class_name(n)

#define class_func(n, a)                                                                                    \
STATIC mp_obj_t n##_make_new(const mp_obj_type_t* type, size_t n_args, size_t n_kw, const mp_obj_t* args);  \
STATIC void n##_query(mp_obj_t self_in, qstr attr, mp_obj_t* dest);                                         \
const mp_obj_type_t class_object(n) =                                                                       \
{                                                                                                           \
    .base = {&mp_type_type},                                                                                \
    .make_new = n##_make_new,                                                                               \
    .print = object_print,                                                                                  \
    .attr = n##_query,                                                                                      \
    .name = MP_QSTR_##n,                                                                                    \
};                                                                                                          \
STATIC mp_obj_t n##_make_new(const mp_obj_type_t* type, size_t n_args, size_t n_kw, const mp_obj_t* args)   \
{                                                                                                           \
    mp_arg_check_num(n_args, n_kw, 0, 0, true);                                                             \
    mp_##n##_t* self = m_new_obj(mp_##n##_t);                                                               \
    self->base.type = &class_object(n);                                                                     \
    return MP_OBJ_FROM_PTR(self);                                                                           \
}                                                                                                           \
STATIC void n##_query(mp_obj_t self_in, qstr attr, mp_obj_t* dest)                                          \
{                                                                                                           \
    mp_##n##_t* self = MP_OBJ_TO_PTR(self_in);                                                              \
    const type_field_info_t* info = find_field_info(qstr_str(attr), a, dim(a));                             \
    if( info )                                                                                              \
    {                                                                                                       \
        if( (dest[0] == MP_OBJ_NULL) && (dest[1] == MP_OBJ_NULL) )                                          \
        {                                                                                                   \
            unsigned int value = 0;                                                                         \
            memcpy(&value, (char*)&self->member + info->offset, info->size);                                \
            dest[0] = mp_obj_new_int_from_uint(value);                                                      \
        }                                                                                                   \
        else if( (dest[0] == MP_OBJ_SENTINEL) && (dest[1] != MP_OBJ_NULL) )                                 \
        {                                                                                                   \
            unsigned int value = mp_obj_get_int(dest[1]);                                                   \
            if( (info->min <= value) && (value <= info->max) )                                              \
            {                                                                                               \
                memcpy((char*)&self->member + info->offset, &value, info->size);                            \
                dest[0] = MP_OBJ_NULL;                                                                      \
            }                                                                                               \
            else                                                                                            \
            {                                                                                               \
                mp_raise_OSError(MP_EINVAL);                                                                \
            }                                                                                               \
        }                                                                                                   \
    }                                                                                                       \
}

#endif
